<script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.11"></script>

<script>
const options = {
  strings: [
    '<pre>Associate Professor</pre>', '<pre>Power Electronics Engineer</pre>', '<pre>IEEE Senior member (PELS)</pre>', '<pre>TPELS Associate Editor</pre>', '<pre>Researcher</pre>', '<pre>JESTPE Associate Editor</pre>', '<pre>Lecturer</pre>'
  ],
  typeSpeed: 60,
  backSpeed: 20,
  cursorChar: '',
  loop: true
};

const typed = new Typed('.typed', options);
const typed2 = new Typed('.typed-eg', options);
</script>
